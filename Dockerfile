FROM alpine
MAINTAINER Kolide Developers <engineering@kolide.co>

RUN apk --update add ca-certificates

COPY ./fleet/linux/fleet ./fleet/linux/fleetctl /usr/bin/
COPY ./cert/server.crt /tmp/server.crt
COPY ./cert/server.key /tmp/server.key
COPY ./wait-for-it.sh /tmp/wait-for-it.sh
COPY ./fleet-start.sh /tmp/fleet-start.sh

CMD ["sh", "/tmp/fleet-start.sh"]
