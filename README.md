# fleet-docker

A complete docker-compose implementation of Kolide Fleet.  Obviously the handling of secrets is naive and would need to be handled differently in production.


Depends on: 

docker
docker-compose

Steps to deploy:

1) clone repository
2) chmod 1777 mysql
3) expose tcp/8080 in firewall
4) docker-compose up

Current versions: 

fleet 2.3.0
maraidb 10.3
redis 3.2.4